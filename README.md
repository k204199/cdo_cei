# cdo_cei
Scripts and Instructions to process ETCCDI compliant Climate Extremes Indices with CDOs
# If you want to use the Jupyter-Notebook locally, run:
./conda env create -f cdo-cei_environment.yaml
# and start jupyter inside this environment
