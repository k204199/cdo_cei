#!/bin/sh
#Program:
cdo=/work/bm0021/cdo_climateindices/cdo_cei/bin/cdo
#
#Switches:
#
#prepareTimeseries=Y
#
#percentiles=Y
csdi=Y
wsdi=Y
#gsl=Y
#cwd=Y
#cdd=Y
#
#Raw directory:
#
root=/work/ik1017/CMIP6/data/CMIP6/
activity_id=CMIP
institution_id=MPI-M
source_id=MPI-ESM1-2-HR
experiment_id=historical
member_id=r1i1p1f1
version=v20190710
#
startyear=1961
endyear=1990
#
#Raw Files:
#
rawTasmax=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/tasmax/gn/${version}/
rawTasmin=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/tasmin/gn/${version}/
rawPr=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/pr/gn/${version}/
#
#Merged timeseries
#
mergedDir=timeseries/
mkdir -p $mergedDir
#
tasmaxMerged=${mergedDir}/tasmax_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tasminMerged=${mergedDir}/tasmin_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
prMerged=${mergedDir}/pr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
#
if [[ $prepareTimeseries ]]; then
  for yearfrag in `seq ${startyear:0:3} 1 ${endyear:0:3}`; do
    mergetermTasmax="${mergetermTasmax} ${rawTasmax}*${yearfrag}*"
    mergetermTasmin="${mergetermTasmin} ${rawTasmin}*${yearfrag}*"
    mergetermPr="${mergetermPr} ${rawPr}*${yearfrag}*"
  done
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermTasmax} ] ${tasmaxMerged}
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermTasmin} ] ${tasminMerged}
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermPr} ] ${prMerged}
fi
#
#Climate indices:
#
cdoOutput=cdooutput/
mkdir -p ${cdoOutput}
#
#percentiles:
tasminrunmin=${cdoOutput}/tasmin_runmin_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tasminrunmax=${cdoOutput}/tasmin_runmax_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tasmaxrunmin=${cdoOutput}/tasmax_runmin_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tasmaxrunmax=${cdoOutput}/tasmax_runmax_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tn10thresh=${cdoOutput}/tn10thresh_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tn90thresh=${cdoOutput}/tn90thresh_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tx10thresh=${cdoOutput}/tx10thresh_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tx90thresh=${cdoOutput}/tx90thresh_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
export CDO_PCTL_NBINS=$((5*(endyear-startyear+1)*2+2))
if [[ $percentiles ]]; then
#  $cdo ydrunmin,5,rm=c $tasminMerged $tasminrunmin
#  $cdo ydrunmax,5,rm=c $tasminMerged $tasminrunmax
#  $cdo ydrunmin,5,rm=c $tasmaxMerged $tasmaxrunmin
#  $cdo ydrunmax,5,rm=c $tasmaxMerged $tasmaxrunmax

  $cdo subc,273.15 -ydrunpctl,10,5,pm=r8,rm=c $tasminMerged ${tasminrunmin} ${tasminrunmax} ${tn10thresh}
  $cdo subc,273.15 -ydrunpctl,90,5,pm=r8,rm=c $tasminMerged ${tasminrunmin} ${tasminrunmax} ${tn90thresh}
  $cdo subc,273.15 -ydrunpctl,10,5,pm=r8,rm=c $tasmaxMerged ${tasmaxrunmin} ${tasmaxrunmax} ${tx10thresh}
  $cdo subc,273.15 -ydrunpctl,90,5,pm=r8,rm=c $tasmaxMerged ${tasmaxrunmin} ${tasmaxrunmax} ${tx90thresh}
fi
#
#cold spell duration index
if [[ $csdi ]]; then
  cdoCsdi=$cdoOutput/csdi_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_cwfi -subc,273.15 ${tasminMerged} $tn10thresh $cdoCsdi
fi
#warm spell duration
if [[ $wsdi ]]; then
  cdoWsdi=$cdoOutput/wsdi_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_hwfi -subc,273.15 ${tasmaxMerged} $tx90thresh $cdoWsdi
fi
#consecutive dry days
if [[ $cdd ]]; then
  cdoWsdi=$cdoOutput/cdd_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_cdd -mulc,86400 ${prMerged} $cdoWsdi
fi
#consecutive wet days
if [[ $cwd ]]; then
  cdoWsdi=$cdoOutput/cwd_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_cwd -mulc,86400 ${prMerged} $cdoWsdi
fi
#thermol growing season length index
if [[ $gsl ]]; then
  cdoGsl=$cdoOutput/gsl_yr_MPI-ESM-LR_historical_r1i1p1_1960-1999.nc
  $cdo etccdi_gsl -divc,2 -add $tasminMerged $tasmaxMerged -gtc,1 $tasmaxMerged $cdoGsl
fi
